java-verbs
==========

Java library to allow functional definition of methods to be shared among unrelated types


```
#!java

describe("a transfer that involves 3 parties: sender, receiver and the transferred object", () -> {

            Sender planetEarth = Sender.create("Earth");
            Receiver moon = Receiver.create("Moon");
            Spaceship apollo11 = Spaceship.create("Apollo11");

            Verbs.in("tests").asFunction("transfer", (object, origin, destination) -> object + " is transferred from " + origin + " to " + destination);

            it("can be expressed with the sender as the subject", () -> {

                assertThat(planetEarth.sendTo(moon, apollo11)).isEqualTo("Apollo11 is transferred from Earth to Moon");
            });

            it("with the receiver as subject", () -> {

                assertThat(moon.receiveFrom(planetEarth, apollo11)).isEqualTo("Apollo11 is transferred from Earth to Moon");
            });

            it("or the object as the subject", () -> {

                assertThat(apollo11.travelBetween(planetEarth, moon)).isEqualTo("Apollo11 is transferred from Earth to Moon");
            });
        });

```

### Maven dependency ###

* Add this repository to your pom:  
```
#!xml
    <repository>
      <id>kfgodel_mosquito</id>
      <name>Repo Mosquito</name>
      <url>http://kfgodel.info:8081/nexus/content/groups/public/</url>
    </repository>
```

* Declare the dependency
```
#!xml

<dependency>
  <groupId>ar.com.dgarcia</groupId>
  <artifactId>java-verbs</artifactId>
  <version>1.0</version>
</dependency>
```