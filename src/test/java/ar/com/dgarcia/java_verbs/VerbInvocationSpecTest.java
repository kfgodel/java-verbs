package ar.com.dgarcia.java_verbs;

import ar.com.dgarcia.java_verbs.api.VerbException;
import ar.com.dgarcia.java_verbs.api.Verbs;
import ar.com.dgarcia.javaspec.api.JavaSpec;
import ar.com.dgarcia.javaspec.api.JavaSpecRunner;
import org.junit.runner.RunWith;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;

/**
 * This type verifies the behavior of verb invocation
 * Created by kfgodel on 10/08/14.
 */

@RunWith(JavaSpecRunner.class)
public class VerbInvocationSpecTest extends JavaSpec<ExampleSpecTest.VerbTestContext> {

    @Override
    public void define() {

        describe("defined with fixed number of arguments", ()->{

            context().verb(()-> Verbs.in("tests").asFunction("test", (onlyArg) -> onlyArg.toString()) );


            it("fails if applied with less args", ()->{
                try {
                    context().verb().apply();
                    failBecauseExceptionWasNotThrown(VerbException.class);
                } catch (VerbException e) {
                    assertThat(e).hasMessage("Verb[test] expected at least 1 arguments but was invoked with 0: []");
                }
            });

            it("fails if applied with more args", ()->{
                try {
                    context().verb().apply("1", "2");
                    failBecauseExceptionWasNotThrown(VerbException.class);
                } catch (VerbException e) {
                    assertThat(e).hasMessage("Verb[test] expected at most 1 arguments but was invoked with 2: [1, 2]");
                }
            });

        });

        describe("defined with variable number of arguments", ()->{

            context().verb(()-> Verbs.in("tests").asFunction("test", (first, second, third, fourth, extra) -> "testing...") );


            it("fails only with less arguments", ()->{
                try {
                    context().verb().apply("1", "2", "3");
                    failBecauseExceptionWasNotThrown(VerbException.class);
                } catch (VerbException e) {
                    assertThat(e).hasMessage("Verb[test] expected at least 4 arguments but was invoked with 3: [1, 2, 3]");
                }

            });
        });


        describe("with wrong arg types", ()->{

            context().verb(()-> Verbs.in("tests").asFunction("test", (String text) -> text) );


            it("fails with a type check reminder", ()->{
                try {
                    context().verb().apply(1);
                    failBecauseExceptionWasNotThrown(VerbException.class);
                } catch (VerbException e) {
                    assertThat(e).hasMessage("Verb[test] failed with ClassCastException. Do argument values match expected types? java.lang.Integer cannot be cast to java.lang.String. Args: [1]");
                }
            });

        });



    }
}
