package ar.com.dgarcia.java_verbs.test_types;

import ar.com.dgarcia.java_verbs.api.Verbs;

/**
 * This type is used to test the verbs
 * Created by kfgodel on 10/08/14.
 */
public class TestObject {

    public String name(){
        return Verbs.in("tests").named("name").invokedOn(this);
    }
}
