package ar.com.dgarcia.java_verbs.test_types;

import ar.com.dgarcia.java_verbs.api.Verbs;

/**
 * Testing type
 * Created by kfgodel on 10/08/14.
 */
public class Dog {

    private String name;

    public static Dog create(String name) {
        Dog dog = new Dog();
        dog.name = name;
        return dog;
    }


    @Override
    public String toString() {
        return name;
    }

    public String playWith(Cat cat) {
        return Verbs.in("tests").named("play").invokedOn(this, cat);
    }
}
