package ar.com.dgarcia.java_verbs.test_types;

import ar.com.dgarcia.java_verbs.api.Verbs;

/**
 * This type serves as a test type
 * Created by kfgodel on 10/08/14.
 */
public class Human {

    private String name;

    public static Human create(String name){
        Human createdHuman = new Human();
        createdHuman.name = name;
        return createdHuman;
    }

    @Override
    public String toString() {
        return name;
    }

    public String playWith(Human other) {
        return Verbs.in("tests").named("play").invokedOn(this, other);
    }

    public String drive(Car aCar) {
        return Verbs.in("tests").named("drive").invokedOn(this, aCar);
    }
}
