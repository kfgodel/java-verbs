package ar.com.dgarcia.java_verbs.test_types;

import ar.com.dgarcia.java_verbs.api.Verbs;

/**
 * Testing type
 * Created by kfgodel on 10/08/14.
 */
public class Car {

    private String name;

    public static Car create(String name) {
        Car car = new Car();
        car.name = name;
        return car;
    }


    @Override
    public String toString() {
        return name;
    }

    public String drivenBy(Human human) {
        return Verbs.in("tests").named("drive").apply(human, this);
    }
}
