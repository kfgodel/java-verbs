package ar.com.dgarcia.java_verbs.test_types;

import ar.com.dgarcia.java_verbs.api.Verbs;

/**
 * Testing type
 * Created by kfgodel on 10/08/14.
 */
public class Spaceship {

    private String name;

    public static Spaceship create(String name) {
        Spaceship ship = new Spaceship();
        ship.name = name;
        return ship;
    }


    @Override
    public String toString() {
        return name;
    }

    public String travelBetween(Sender sender, Receiver receiver) {
        return Verbs.in("tests").named("transfer").apply(this,sender, receiver);
    }
}
