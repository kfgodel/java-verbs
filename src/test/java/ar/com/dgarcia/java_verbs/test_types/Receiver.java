package ar.com.dgarcia.java_verbs.test_types;

import ar.com.dgarcia.java_verbs.api.Verbs;

/**
 * Testing type
 * Created by kfgodel on 10/08/14.
 */
public class Receiver {

    private String name;

    public static Receiver create(String name) {
        Receiver receiver = new Receiver();
        receiver.name = name;
        return receiver;
    }


    @Override
    public String toString() {
        return name;
    }

    public String receiveFrom(Sender sender, Spaceship spaceship) {
        return Verbs.in("tests").named("transfer").apply(spaceship,sender, this);
    }
}
