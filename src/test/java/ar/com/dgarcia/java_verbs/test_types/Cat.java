package ar.com.dgarcia.java_verbs.test_types;

import ar.com.dgarcia.java_verbs.api.Verbs;

/**
 * Testing type
 * Created by kfgodel on 10/08/14.
 */
public class Cat {

    private String name;

    public static Cat create(String name) {
        Cat cat = new Cat();
        cat.name = name;
        return cat;
    }


    @Override
    public String toString() {
        return name;
    }

    public String playWith(Dog dog) {
        return Verbs.in("tests").named("play").invokedOn(this, dog);
    }
}
