package ar.com.dgarcia.java_verbs.test_types;

import ar.com.dgarcia.java_verbs.api.Verbs;

/**
 * Testing type
 * Created by kfgodel on 10/08/14.
 */
public class Sender {

    private String name;

    public static Sender create(String name) {
        Sender sender = new Sender();
        sender.name = name;
        return sender;
    }


    @Override
    public String toString() {
        return name;
    }

    public String sendTo(Receiver receiver, Spaceship spaceship) {
        return Verbs.in("tests").named("transfer").apply(spaceship, this, receiver);
    }
}
