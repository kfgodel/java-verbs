package ar.com.dgarcia.java_verbs;

import ar.com.dgarcia.java_verbs.api.Verb;
import ar.com.dgarcia.java_verbs.api.Verbs;
import ar.com.dgarcia.javaspec.api.JavaSpec;
import ar.com.dgarcia.javaspec.api.JavaSpecRunner;
import ar.com.dgarcia.javaspec.api.TestContext;
import org.junit.runner.RunWith;

import java.util.function.Supplier;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

/**
 * This type verifies all the verb definition possibilities
 * Created by kfgodel on 09/08/14.
 */
@RunWith(JavaSpecRunner.class)
public class VerbDefinitionSpecTest extends JavaSpec<VerbDefinitionSpecTest.VerbDefinitionSpecContext> {


    public static interface VerbDefinitionSpecContext extends TestContext {

        void operation(Supplier<TestOperation> definition);
        TestOperation operation();
    }

    public static interface TestOperation {

        void operationWithNoReturn(Object... args);

        String operationWithReturn(Object... args);

    }

    @Override
    public void define() {

        context().operation(()-> mock(TestOperation.class));

        describe("a verb with no return value", () -> {

            it("and no argument", () -> {
                Verb verb = Verbs.in("test").asAction("test", () -> context().operation().operationWithNoReturn());

                verb.apply();

                verify(context().operation()).operationWithNoReturn();
            });

            it("1 argument", () -> {
                Verb verb = Verbs.in("test").asAction("test", (first) -> context().operation().operationWithNoReturn(first));

                verb.apply("1");

                verify(context().operation()).operationWithNoReturn("1");
            });

            it("2 arguments", () -> {
                Verb verb = Verbs.in("test").asAction("test", (first, second) -> context().operation().operationWithNoReturn(first, second));

                verb.apply("1", "2");

                verify(context().operation()).operationWithNoReturn("1", "2");
            });

            it("3 arguments", () -> {
                Verb verb = Verbs.in("test").asAction("test", (first, second, third) -> context().operation().operationWithNoReturn(first, second, third));

                verb.apply("1", "2", "3");

                verify(context().operation()).operationWithNoReturn("1", "2", "3");
            });

            it("4 arguments", () -> {
                Verb verb = Verbs.in("test").asAction("test", (first, second, third, fourth, extra) -> context().operation().operationWithNoReturn(first, second, third, fourth));

                verb.apply("1", "2", "3", "4");

                verify(context().operation()).operationWithNoReturn("1", "2", "3", "4");
            });

            it("5 or more arguments", () -> {
                Verb verb = Verbs.in("test").asAction("test", (first, second, third, fourth, extra) -> context().operation().operationWithNoReturn(first, second, third, fourth, extra[0]));

                verb.apply("1", "2", "3", "4", "5");

                verify(context().operation()).operationWithNoReturn("1", "2", "3", "4", "5");
            });

        });

        describe("a verb with return value", ()->{

            it("and no argument", ()->{
                Verb verb = Verbs.in("test").asFunction("test", () -> context().operation().operationWithReturn());

                verb.apply();

                verify(context().operation()).operationWithReturn();
            });

            it("1 argument", ()->{
                Verb verb = Verbs.in("test").asFunction("test", (first) -> context().operation().operationWithReturn(first));

                verb.apply("1");

                verify(context().operation()).operationWithReturn("1");
            });

            it("2 arguments", ()->{
                Verb verb = Verbs.in("test").asFunction("test", (first, second) -> context().operation().operationWithReturn(first, second));

                verb.apply("1", "2");

                verify(context().operation()).operationWithReturn("1","2");
            });

            it("3 arguments", ()->{
                Verb verb = Verbs.in("test").asFunction("test", (first, second, third) -> context().operation().operationWithReturn(first, second, third));

                verb.apply("1", "2", "3");

                verify(context().operation()).operationWithReturn("1","2", "3");
            });

            it("4 arguments", ()->{
                Verb verb = Verbs.in("test").asFunction("test", (first, second, third, fourth, extra) -> context().operation().operationWithReturn(first, second, third, fourth));

                verb.apply("1", "2", "3", "4");

                verify(context().operation()).operationWithReturn("1", "2", "3", "4");
            });

            it("5 or more arguments", ()->{
                Verb verb = Verbs.in("test").asFunction("test", (first, second, third, fourth, extra) -> context().operation().operationWithReturn(first, second, third, fourth, extra[0]));

                verb.apply("1", "2", "3", "4", "5");

                verify(context().operation()).operationWithReturn("1", "2", "3", "4", "5");
            });
        });

    }

}
