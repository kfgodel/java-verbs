package ar.com.dgarcia.java_verbs;

import ar.com.dgarcia.java_verbs.api.Verb;
import ar.com.dgarcia.java_verbs.api.Verbs;
import ar.com.dgarcia.java_verbs.test_types.*;
import ar.com.dgarcia.javaspec.api.JavaSpec;
import ar.com.dgarcia.javaspec.api.JavaSpecRunner;
import ar.com.dgarcia.javaspec.api.TestContext;
import org.junit.runner.RunWith;

import java.util.function.Supplier;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * This type defines exemplary specs to show JavaVerbs capabilities
 * Created by kfgodel on 10/08/14.
 */
@RunWith(JavaSpecRunner.class)
public class ExampleSpecTest extends JavaSpec<TestContext> {

    public static interface VerbTestContext extends TestContext {
        Verb verb();
        void verb(Supplier<Verb> definition);
    }

    @Override
    public void define() {

        describe("a piece of behavior", ()->{

            Verb verb = Verbs.in("tests").asFunction("name", () -> "John Smith");

            it("is a function defined in a namespace", () -> {
                String returnedValue = verb.apply();

                assertThat(returnedValue).isEqualTo("John Smith");
            });

            it("that can be used to implement a method", () -> {
                TestObject testObject = new TestObject();

                assertThat(testObject.name()).isEqualTo("John Smith");
            });

        });

        describe("functions can be shared", () -> {

            Verbs.in("tests").asFunction("play", (first, second) -> first + " is playing with " + second);

            it("between types with a common ancestor", () -> {
                Human brother = Human.create("Hansel");
                Human sister = Human.create("Gretel");

                assertThat(brother.playWith(sister)).isEqualTo("Hansel is playing with Gretel");
                assertThat(sister.playWith(brother)).isEqualTo("Gretel is playing with Hansel");
            });

            it("between unrelated types", () -> {
                Cat garfield = Cat.create("Garfield");
                Dog odie = Dog.create("Odie");

                assertThat(garfield.playWith(odie)).isEqualTo("Garfield is playing with Odie");
                assertThat(odie.playWith(garfield)).isEqualTo("Odie is playing with Garfield");
            });

        });

        describe("when sharing among unrelated types", () -> {

            Verbs.in("tests").asFunction("drive", (person, car) -> person + " is driving " + car);

            it("method names can differ to improve expressiveness", () -> {
                Human schummy = Human.create("Schumacher");
                Car fantasticCar = Car.create("KITT");

                assertThat(schummy.drive(fantasticCar)).isEqualTo("Schumacher is driving KITT");
                assertThat(fantasticCar.drivenBy(schummy)).isEqualTo("Schumacher is driving KITT");
            });

        });

        describe("a transfer that involves 3 parties: sender, receiver and the transferred object", () -> {

            Sender planetEarth = Sender.create("Earth");
            Receiver moon = Receiver.create("Moon");
            Spaceship apollo11 = Spaceship.create("Apollo11");

            Verbs.in("tests").asFunction("transfer", (object, origin, destination) -> object + " is transferred from " + origin + " to " + destination);

            it("can be expressed with the sender as the subject", () -> {

                assertThat(planetEarth.sendTo(moon, apollo11)).isEqualTo("Apollo11 is transferred from Earth to Moon");
            });

            it("with the receiver as subject", () -> {

                assertThat(moon.receiveFrom(planetEarth, apollo11)).isEqualTo("Apollo11 is transferred from Earth to Moon");
            });

            it("or the object as the subject", () -> {

                assertThat(apollo11.travelBetween(planetEarth, moon)).isEqualTo("Apollo11 is transferred from Earth to Moon");
            });
        });

        describe("behavior", ()->{

            Verb verb = Verbs.in("tests").asFunction("salute", () -> "Hello");


            it("can be re-defined in runtime", ()->{
                assertThat(verb.<String>apply()).isEqualTo("Hello");

                Verbs.in("tests").asFunction("salute", () -> "Bye");

                assertThat(verb.<String>apply()).isEqualTo("Bye");
            });

        });

    }
}
