package ar.com.dgarcia.java_verbs.impl.lambda.variations;

import java.util.function.Consumer;

/**
 * This type represents a lambda that's defined as a consumer instance
 * Created by kfgodel on 11/08/14.
 */
public class ConsumerLambda extends LambdaSupport {
    private Consumer<Object> consumer;

    @Override
    protected <T> T invokeWith(Object[] args) {
        consumer.accept(args[0]);
        return null;
    }

    @Override
    public int getMinimumArgCount() {
        return 1;
    }

    @Override
    public int getMaximumArgCount() {
        return 1;
    }

    public static ConsumerLambda create(Consumer<?> consumer) {
        ConsumerLambda lambda = new ConsumerLambda();
        lambda.consumer = (Consumer<Object>) consumer;
        return lambda;
    }

}
