package ar.com.dgarcia.java_verbs.impl.lambda.variations;

/**
 * This type represents a lambda created as a Runnable
 * Created by kfgodel on 11/08/14.
 */
public class RunnableLambda extends LambdaSupport {

    private Runnable runnable;

    @Override
    public <T> T invokeWith(Object... args) {
        runnable.run();
        return null;
    }

    @Override
    public int getMinimumArgCount() {
        return 0;
    }

    @Override
    public int getMaximumArgCount() {
        return 0;
    }

    public static RunnableLambda create(Runnable runnable) {
        RunnableLambda lambda = new RunnableLambda();
        lambda.runnable = runnable;
        return lambda;
    }

}
