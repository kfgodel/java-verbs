package ar.com.dgarcia.java_verbs.impl.lambda;

import ar.com.dgarcia.java_verbs.api.VerbException;

/**
 * This type represents an error on lambda invocations
 * Created by kfgodel on 11/08/14.
 */
public class LambdaException extends VerbException {

    public LambdaException(String message) {
        super(message);
    }

    public LambdaException(String message, Throwable cause) {
        super(message, cause);
    }

    public LambdaException(Throwable cause) {
        super(cause);
    }
}
