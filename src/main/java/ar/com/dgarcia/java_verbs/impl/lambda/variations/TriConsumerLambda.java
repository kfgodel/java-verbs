package ar.com.dgarcia.java_verbs.impl.lambda.variations;

import ar.com.dgarcia.java_verbs.api.lambdas.TriConsumer;

/**
 * This type represents a lambda that's defined as a tri-consumer instance
 * Created by kfgodel on 11/08/14.
 */
public class TriConsumerLambda extends LambdaSupport {
    private TriConsumer<Object, Object, Object> consumer;

    @Override
    protected <T> T invokeWith(Object[] args) {
        consumer.accept(args[0], args[1], args[2]);
        return null;
    }

    @Override
    public int getMinimumArgCount() {
        return 3;
    }

    @Override
    public int getMaximumArgCount() {
        return 3;
    }

    public static TriConsumerLambda create(TriConsumer<?, ?, ?> consumer) {
        TriConsumerLambda lambda = new TriConsumerLambda();
        lambda.consumer = (TriConsumer<Object, Object, Object>) consumer;
        return lambda;
    }

}
