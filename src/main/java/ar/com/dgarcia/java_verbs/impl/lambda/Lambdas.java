package ar.com.dgarcia.java_verbs.impl.lambda;

import ar.com.dgarcia.java_verbs.api.lambdas.*;
import ar.com.dgarcia.java_verbs.impl.lambda.variations.*;

import java.util.function.*;

/**
 * This type represents an access point to create generic lambdas
 * Created by kfgodel on 11/08/14.
 */
public class Lambdas {

    /**
     * Creates a lambda from a runnable instance
     * @param runnable The lambda definition
     * @return The appropriate lambda for a runnable
     */
    public static Lambda fromRunnable( Runnable runnable) {
        return RunnableLambda.create(runnable);
    }

    /**
     * Creates a lambda from a Supplier instance
     * @param supplierFunction The lambda definition
     * @return The appropriate lambda for a supplier
     */
    public static <T> Lambda fromProducer(Supplier<T> supplierFunction) {
        return SupplierLambda.create(supplierFunction);
    }

    /**
     * Creates a lambda from a Consumer instance
     * @param consumerFunction The lambda definition
     * @return The appropriate lambda for a Consumer
     */
    public static <T> Lambda fromConsumer( Consumer<T> consumerFunction) {
        return ConsumerLambda.create(consumerFunction);
    }

    /**
     * Creates a lambda from a Function instance
     * @param function The lambda definition
     * @return The appropriate lambda for a Function
     */
    public static <T, R> Lambda fromProducer(Function<T, R> function) {
        return FunctionLambda.create(function);
    }

    /**
     * Creates a lambda from a BiConsumer instance
     * @param consumerFunction The lambda definition
     * @return The appropriate lambda for a BiConsumer
     */
    public static <T, U> Lambda fromConsumer( BiConsumer<T, U> consumerFunction) {
        return BiConsumerLambda.create(consumerFunction);
    }

    /**
     * Creates a lambda from a BiFunction instance
     * @param biFunction The lambda definition
     * @return The appropriate lambda for a BiFunction
     */
    public static <T, U, R> Lambda fromProducer( BiFunction<T, U, R> biFunction) {
        return BiFunctionLambda.create(biFunction);
    }

    /**
     * Creates a lambda from a TriConsumer instance
     * @param consumerFunction The lambda definition
     * @return The appropriate lambda for a TriConsumer
     */
    public static <T, U, V> Lambda fromConsumer( TriConsumer<T, U, V> consumerFunction) {
        return TriConsumerLambda.create(consumerFunction);
    }

    /**
     * Creates a lambda from a TriFunction instance
     * @param triFunction The lambda definition
     * @return The appropriate lambda for a TriFunction
     */
    public static <T, U, V, R> Lambda fromProducer( TriFunction<T, U, V, R> triFunction) {
        return TriFunctionLambda.create(triFunction);
    }

    /**
     * Creates a lambda from a TetraOrMoreConsumer instance
     * @param consumerFunction The lambda definition
     * @return The appropriate lambda for a TetraOrMoreConsumer
     */
    public static <T, U, V, W, X> Lambda fromConsumer( TetraOrMoreConsumer<T, U, V, W, X> consumerFunction) {
        return TetraOrMoreConsumerLambda.create(consumerFunction);
    }

    /**
     * Creates a lambda from a TetraOrMoreFunction instance
     * @param tetraFunction The lambda definition
     * @return The appropriate lambda for a TetraOrMoreFunction
     */
    public static <T, U, V, W, X, R> Lambda fromProducer( TetraOrMoreFunction<T, U, V, W, X, R> tetraFunction) {
        return TetraOrMoreFunctionLambda.create(tetraFunction);
    }
}
