package ar.com.dgarcia.java_verbs.impl.lambda.variations;

import ar.com.dgarcia.java_verbs.api.lambdas.TetraOrMoreConsumer;

/**
 * This type represents a lambda that's defined as a tetra-consumer instance
 * Created by kfgodel on 11/08/14.
 */
public class TetraOrMoreConsumerLambda extends LambdaSupport {
    private TetraOrMoreConsumer<Object, Object, Object, Object, Object> consumer;

    @Override
    protected <T> T invokeWith(Object[] args) {
        Object[] remainingArgs = this.getRemainingArgsAfterMinimum(args);
        consumer.accept(args[0], args[1], args[2], args[3], remainingArgs);
        return null;
    }

    @Override
    public int getMinimumArgCount() {
        return 4;
    }

    @Override
    public int getMaximumArgCount() {
        return Integer.MAX_VALUE;
    }

    public static TetraOrMoreConsumerLambda create(TetraOrMoreConsumer<?, ?, ?, ?, ?> consumer) {
        TetraOrMoreConsumerLambda lambda = new TetraOrMoreConsumerLambda();
        lambda.consumer = (TetraOrMoreConsumer<Object, Object, Object, Object, Object>) consumer;
        return lambda;
    }

}
