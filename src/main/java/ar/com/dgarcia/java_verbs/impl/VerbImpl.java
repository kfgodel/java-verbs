package ar.com.dgarcia.java_verbs.impl;

import ar.com.dgarcia.java_verbs.api.Verb;
import ar.com.dgarcia.java_verbs.api.VerbException;
import ar.com.dgarcia.java_verbs.api.VerbNamespace;
import ar.com.dgarcia.java_verbs.api.lambdas.Lambda;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * This type represents a verb
 * Created by kfgodel on 11/08/14.
 */
public class VerbImpl implements Verb {

    private static final Object[] emptyArray = new Object[0];

    private Lambda verbDefinition;

    private String name;

    @Override
    public <T> T apply(Object... args) throws VerbException {
        int currentArgCount = (args == null)? 0 : args.length;
        int minimumArgCount = verbDefinition.getMinimumArgCount();
        if(currentArgCount < minimumArgCount){
            throw new VerbException("Verb["+name+"] expected at least "+ minimumArgCount + " arguments but was invoked with "+currentArgCount+": " + Arrays.toString(args));
        }

        int maximumArgCount = verbDefinition.getMaximumArgCount();
        if(currentArgCount > maximumArgCount){
            throw new VerbException("Verb["+name+"] expected at most "+ maximumArgCount + " arguments but was invoked with "+currentArgCount+": " + Arrays.toString(args));
        }

        try {
            return verbDefinition.apply(args);
        } catch (ClassCastException e) {
            throw new VerbException("Verb["+name+"] failed with ClassCastException. Do argument values match expected types? "+e.getMessage()+ ". Args: " + Arrays.toString(args));
        }
    }

    @Override
    public <T> T invokedOn(Object hostInstance, Object... args) throws VerbException {
        //If no extra args, we receive null instead of an empty array
        if(args == null){
            args = emptyArray;
        }

        List<Object> argsAsList = new ArrayList<>(1  + args.length);
        //In case we cannot even pass this as first arg
        if(verbDefinition.getMaximumArgCount() > 0){
            argsAsList.add(hostInstance);
            for (Object arg : args) {
                argsAsList.add(arg);
            }
        }
        Object[] expandedArgs = argsAsList.toArray();
        return apply(expandedArgs);
    }

    @Override
    public void redefine(Lambda newVerbDefinition) {
        if(newVerbDefinition == null){
            throw new IllegalArgumentException("A verb definition cannot be null");
        }
        this.verbDefinition = newVerbDefinition;
    }

    public static VerbImpl create(String name, Lambda definition) {
        VerbImpl verb = new VerbImpl();
        verb.verbDefinition = definition;
        verb.name = name;
        return verb;
    }

}
