package ar.com.dgarcia.java_verbs.impl.lambda.variations;

import java.util.function.Function;

/**
 * This type represents a lambda that's defined as a function instance
 * Created by kfgodel on 11/08/14.
 */
public class FunctionLambda extends LambdaSupport {
    private Function<Object, ?> function;

    @Override
    protected <T> T invokeWith(Object[] args) {
        return (T) function.apply(args[0]);
    }

    @Override
    public int getMinimumArgCount() {
        return 1;
    }

    @Override
    public int getMaximumArgCount() {
        return 1;
    }

    public static FunctionLambda create(Function<?, ?> function) {
        FunctionLambda lambda = new FunctionLambda();
        lambda.function = (Function<Object, ?>) function;
        return lambda;
    }

}
