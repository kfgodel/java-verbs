package ar.com.dgarcia.java_verbs.impl.lambda.variations;

import java.util.function.BiFunction;

/**
 * This type represents a lambda that's defined as a bi-function instance
 * Created by kfgodel on 11/08/14.
 */
public class BiFunctionLambda extends LambdaSupport {
    private BiFunction<Object, Object, Object> function;

    @Override
    protected <T> T invokeWith(Object[] args) {
        return (T) function.apply(args[0], args[1]);
    }

    @Override
    public int getMinimumArgCount() {
        return 2;
    }

    @Override
    public int getMaximumArgCount() {
        return 2;
    }

    public static BiFunctionLambda create(BiFunction<?, ?, ?> function) {
        BiFunctionLambda lambda = new BiFunctionLambda();
        lambda.function = (BiFunction<Object, Object, Object>) function;
        return lambda;
    }

}
