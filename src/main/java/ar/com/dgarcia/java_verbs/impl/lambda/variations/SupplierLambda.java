package ar.com.dgarcia.java_verbs.impl.lambda.variations;

import java.util.function.Supplier;

/**
 * This type represents a lambda defined as a supplier instance
 * Created by kfgodel on 11/08/14.
 */
public class SupplierLambda extends LambdaSupport {

    private Supplier<?> supplier;

    @Override
    protected <T> T invokeWith(Object[] args) {
        return (T) supplier.get();
    }

    @Override
    public int getMinimumArgCount() {
        return 0;
    }

    @Override
    public int getMaximumArgCount() {
        return 0;
    }

    public static SupplierLambda create(Supplier<?> supplier) {
        SupplierLambda lambda = new SupplierLambda();
        lambda.supplier = supplier;
        return lambda;
    }

}
