package ar.com.dgarcia.java_verbs.impl.lambda.variations;

import java.util.function.BiConsumer;

/**
 * This type represents a lambda that's defined as a bi-consumer instance
 * Created by kfgodel on 11/08/14.
 */
public class BiConsumerLambda extends LambdaSupport {
    private BiConsumer<Object, Object> consumer;

    @Override
    protected <T> T invokeWith(Object[] args) {
        consumer.accept(args[0], args[1]);
        return null;
    }

    @Override
    public int getMinimumArgCount() {
        return 2;
    }

    @Override
    public int getMaximumArgCount() {
        return 2;
    }

    public static BiConsumerLambda create(BiConsumer<?, ?> consumer) {
        BiConsumerLambda lambda = new BiConsumerLambda();
        lambda.consumer = (BiConsumer<Object, Object>) consumer;
        return lambda;
    }

}
