package ar.com.dgarcia.java_verbs.impl.lambda.variations;

import ar.com.dgarcia.java_verbs.api.lambdas.TetraOrMoreConsumer;
import ar.com.dgarcia.java_verbs.api.lambdas.TetraOrMoreFunction;

/**
 * This type represents a lambda that's defined as a tetra-function instance
 * Created by kfgodel on 11/08/14.
 */
public class TetraOrMoreFunctionLambda extends LambdaSupport {
    private TetraOrMoreFunction<Object, Object, Object, Object, Object, ?> function;

    @Override
    protected <T> T invokeWith(Object[] args) {
        Object[] remainingArgs = this.getRemainingArgsAfterMinimum(args);
        return (T) function.apply(args[0],args[1],args[2],args[3],remainingArgs);
    }

    @Override
    public int getMinimumArgCount() {
        return 4;
    }

    @Override
    public int getMaximumArgCount() {
        return Integer.MAX_VALUE;
    }

    public static TetraOrMoreFunctionLambda create(TetraOrMoreFunction<?,?,?,?,?,?> function) {
        TetraOrMoreFunctionLambda lambda = new TetraOrMoreFunctionLambda();
        lambda.function = (TetraOrMoreFunction<Object, Object, Object, Object, Object, ?>) function;
        return lambda;
    }

}
