package ar.com.dgarcia.java_verbs.impl.lambda.variations;

import ar.com.dgarcia.java_verbs.api.lambdas.TriFunction;

/**
 * This type represents a lambda that's defined as a tri-function instance
 * Created by kfgodel on 11/08/14.
 */
public class TriFunctionLambda extends LambdaSupport {
    private TriFunction<Object, Object, Object, ?> function;

    @Override
    protected <T> T invokeWith(Object[] args) {
        return (T) function.apply(args[0], args[1], args[2]);
    }

    @Override
    public int getMinimumArgCount() {
        return 3;
    }

    @Override
    public int getMaximumArgCount() {
        return 3;
    }

    public static TriFunctionLambda create(TriFunction<?, ?, ?, ?> function) {
        TriFunctionLambda lambda = new TriFunctionLambda();
        lambda.function = (TriFunction<Object, Object, Object, ?>) function;
        return lambda;
    }

}
