package ar.com.dgarcia.java_verbs.impl;

import ar.com.dgarcia.java_verbs.api.Verb;
import ar.com.dgarcia.java_verbs.api.VerbException;
import ar.com.dgarcia.java_verbs.api.VerbNamespace;
import ar.com.dgarcia.java_verbs.api.lambdas.*;
import ar.com.dgarcia.java_verbs.impl.lambda.Lambdas;

import java.util.TreeMap;
import java.util.function.*;

/**
 * This type represents a ver namespace
 * Created by kfgodel on 11/08/14.
 */
public class VerbNamespaceImpl implements VerbNamespace {

    private TreeMap<String, VerbNamespace> subspaces;

    private TreeMap<String, Verb> verbs;

    public static VerbNamespaceImpl create() {
        VerbNamespaceImpl namespace = new VerbNamespaceImpl();
        namespace.subspaces = new TreeMap<>();
        namespace.verbs = new TreeMap<>();
        return namespace;
    }

    @Override
    public <T> Verb asFunction(String functionName, Supplier<T> supplierFunction) {
        Lambda verbDefinition = Lambdas.fromProducer(supplierFunction);
        return defineVerb(functionName, verbDefinition);
    }

    @Override
    public <T, R> Verb asFunction(String functionName, Function<T, R> function) {
        Lambda verbDefinition = Lambdas.fromProducer(function);
        return defineVerb(functionName, verbDefinition);
    }

    @Override
    public <T, U, R> Verb asFunction(String functionName, BiFunction<T, U, R> biFunction) {
        Lambda verbDefinition = Lambdas.fromProducer(biFunction);
        return defineVerb(functionName, verbDefinition);
    }

    @Override
    public <T, U, V, R> Verb asFunction(String functionName, TriFunction<T, U, V, R> triFunction) {
        Lambda verbDefinition = Lambdas.fromProducer(triFunction);
        return defineVerb(functionName, verbDefinition);
    }

    @Override
    public <T, U, V, W, X, R> Verb asFunction(String functionName, TetraOrMoreFunction<T, U, V, W, X, R> tetraFunction) {
        Lambda verbDefinition = Lambdas.fromProducer(tetraFunction);
        return defineVerb(functionName, verbDefinition);
    }

    @Override
    public Verb asAction(String functionName, Runnable runnable) {
        Lambda verbDefinition = Lambdas.fromRunnable(runnable);
        return defineVerb(functionName, verbDefinition);
    }

    @Override
    public <T> Verb asAction(String functionName, Consumer<T> consumerFunction) {
        Lambda verbDefinition = Lambdas.fromConsumer(consumerFunction);
        return defineVerb(functionName, verbDefinition);
    }

    @Override
    public <T, U> Verb asAction(String functionName, BiConsumer<T, U> consumerFunction) {
        Lambda verbDefinition = Lambdas.fromConsumer(consumerFunction);
        return defineVerb(functionName, verbDefinition);
    }

    @Override
    public <T, U, V> Verb asAction(String functionName, TriConsumer<T, U, V> consumerFunction) {
        Lambda verbDefinition = Lambdas.fromConsumer(consumerFunction);
        return defineVerb(functionName, verbDefinition);
    }

    @Override
    public <T, U, V, W, X> Verb asAction(String functionName, TetraOrMoreConsumer<T, U, V, W, X> consumerFunction) {
        Lambda verbDefinition = Lambdas.fromConsumer(consumerFunction);
        return defineVerb(functionName, verbDefinition);
    }

    @Override
    public Verb named(String functionName) {
        Verb verb = verbs.get(functionName);
        if(verb == null){
            throw new VerbException("The requested verb["+functionName+"] does not exists");
        }
        return verb;
    }

    /**
     * Redefines or creates a new ver with the given name under this namespace
     * @param functionName The name for the verb
     * @param verbDefinition The new definition
     * @return The existing or created verb
     */
    private Verb defineVerb(String functionName, Lambda verbDefinition) {
        Verb existingVerb = verbs.get(functionName);
        if(existingVerb != null){
            existingVerb.redefine(verbDefinition);
            return existingVerb;
        }
        VerbImpl createdVerb = VerbImpl.create(functionName, verbDefinition);
        verbs.put(functionName, createdVerb);
        return createdVerb;
    }


    @Override
    public VerbNamespace subSpace(String namespaceName) {
        VerbNamespace existingNamespace = subspaces.get(namespaceName);
        if(existingNamespace != null){
            return existingNamespace;
        }
        VerbNamespaceImpl createdNamespace = VerbNamespaceImpl.create();
        subspaces.put(namespaceName, createdNamespace);
        return createdNamespace;
    }
}
