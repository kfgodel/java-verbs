package ar.com.dgarcia.java_verbs.impl.lambda.variations;

import ar.com.dgarcia.java_verbs.api.lambdas.Lambda;
import ar.com.dgarcia.java_verbs.impl.lambda.LambdaException;

import java.util.Arrays;

/**
 * This type serves as superclass with common behavior for lambdas
 * Created by kfgodel on 11/08/14.
 */
public abstract class LambdaSupport implements Lambda{

    @Override
    public <T> T apply(Object... args) throws LambdaException {
        verifyArgs(args);
        return invokeWith(args);
    }

    /**
     * Subclass responsibility. Invokes the actual piece of behavior
     * @param args The validated args according to this lambda definition
     * @param <T> The type of expected result
     * @return The returned value
     */
    protected abstract <T> T invokeWith(Object[] args);

    /**
     * Verifies that the current args are under expected limits
     * @param args The current args
     */
    private void verifyArgs(Object[] args) throws LambdaException {
        int actualArgCount = 0;
        if(args != null){
            actualArgCount = args.length;
        }
        int minimumArgCount = getMinimumArgCount();
        int maximumArgCount = getMaximumArgCount();
        if(actualArgCount < minimumArgCount || actualArgCount > maximumArgCount){
            throw new LambdaException("Lambda expected ["+minimumArgCount+", "+maximumArgCount+"] args. Cannot be invoked with: " + Arrays.toString(args));
        }
    }

    /**
     * Returns an array with the arguments in the indexes after the minimum args.<br>
     *     If args is null, then null is returned.<br>
     *     If there's no remaining arg, then null is returned
     * @param args The actual arguments
     * @return The array of the last par after remaining
     */
    public Object[] getRemainingArgsAfterMinimum(Object[] args){
        if(args == null){
            return null;
        }
        int minimumArgCount = getMinimumArgCount();
        int actualCount = args.length;
        if(actualCount < minimumArgCount){
            throw new LambdaException("Cannot get remaining args when actual count["+actualCount+"] is less than minimum[" + minimumArgCount +"]");
        }
        if(actualCount == minimumArgCount){
            return null;
        }
        Object[] remaining = Arrays.copyOfRange(args, minimumArgCount, actualCount);
        return remaining;
    }
}
