package ar.com.dgarcia.java_verbs.api;

import ar.com.dgarcia.java_verbs.impl.VerbNamespaceImpl;

import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * This type serves as a static entry point for verbs declaration and utilization
 * Created by kfgodel on 10/08/14.
 */
public class Verbs {

    /**
     * This serves as a global root namespace
     */
    private static final VerbNamespace rootNamespace = VerbNamespaceImpl.create();


    /**
     * Returns the verb namespace identified by the given name
     * @param namespaceName The namespace identifier
     * @return The created or existing namespace
     */
    public static VerbNamespace in(String namespaceName) {
        return rootNamespace.subSpace(namespaceName);
    }
}
