package ar.com.dgarcia.java_verbs.api;

import ar.com.dgarcia.java_verbs.api.lambdas.TetraOrMoreConsumer;
import ar.com.dgarcia.java_verbs.api.lambdas.TetraOrMoreFunction;
import ar.com.dgarcia.java_verbs.api.lambdas.TriConsumer;
import ar.com.dgarcia.java_verbs.api.lambdas.TriFunction;

import java.util.function.*;

/**
 * This type represents a verb namespace tha can store verbs in it
 * Created by kfgodel on 09/08/14.
 */
public interface VerbNamespace {
    /**
     * Registers a new function with the given name that returns a value without an argument
     * @param functionName The name to identify the function
     * @param supplierFunction The function definition as a supplier instance
     * @return The verb that was created
     */
    <T> Verb asFunction(String functionName, Supplier<T> supplierFunction);

    /**
     * Registers a new function with the given name that returns a value and takes one arguments
     * @param functionName The name to identify the function
     * @param function The function definition
     * @return The created verb
     */
    <T,R> Verb asFunction(String functionName, Function<T, R> function);

    /**
     * Registers a new function with the given name that returns a value and takes two arguments
     * @param functionName The name to identify the function
     * @param biFunction The function definition
     * @return The created verb
     */
    <T, U, R> Verb asFunction(String functionName, BiFunction<T,U,R> biFunction);

    /**
     * Registers a new function with the given name that returns a value and takes 3 arguments
     * @param functionName The function that identifies the function
     * @param triFunction The function definition
     * @return The created verb
     */
    <T, U, V, R> Verb asFunction(String functionName, TriFunction<T, U, V, R> triFunction);

    /**
     * Registers a new function with the given name that returns a value and takes 4 or more arguments
     * @param functionName The function that identifies the function
     * @param tetraFunction The function definition
     * @return The created verb
     */
    <T, U, V, W, X, R> Verb asFunction(String functionName, TetraOrMoreFunction<T, U, V, W, X, R> tetraFunction);

    /**
     * Registers a new function with the given name that has no return value and no argument
     * @param functionName The name to identify the function
     * @param runnable The function definition as a Runnable instance
     * @return The verb that was created
     */
    Verb asAction(String functionName, Runnable runnable);

    /**
     * Registers a new function with the given name that has no return value and 1 argument
     * @param functionName The name to identify the function
     * @param consumerFunction The function definition as a consumer instance
     * @return The verb that was created
     */
    <T> Verb asAction(String functionName, Consumer<T> consumerFunction);

    /**
     * Registers a new function with the given name that has no return value and 2 argument
     * @param functionName The name to identify the function
     * @param consumerFunction The function definition as a consumer instance
     * @return The verb that was created
     */
    <T, U> Verb asAction(String functionName, BiConsumer<T, U> consumerFunction);

    /**
     * Registers a new function with the given name that has no return value and 3 argument
     * @param functionName The name to identify the function
     * @param consumerFunction The function definition as a consumer instance
     * @return The verb that was created
     */
    <T, U, V> Verb asAction(String functionName, TriConsumer<T, U, V> consumerFunction);

    /**
     * Registers a new function with the given name that has no return value and 4 or more argument
     * @param functionName The name to identify the function
     * @param consumerFunction The function definition as a consumer instance
     * @return The verb that was created
     */
    <T, U, V, W, X> Verb asAction(String functionName, TetraOrMoreConsumer<T, U, V, W, X> consumerFunction);

    /**
     * Returns the verb created with the given name
     * @param functionName The name to identify the created function
     * @return The verb created
     */
    Verb named(String functionName);

    /**
     * Gets the subspace under this with the given name
     * @param namespaceName The name that identifies the space as a child
     * @return The created or existing namespace
     */
    VerbNamespace subSpace(String namespaceName);
}
