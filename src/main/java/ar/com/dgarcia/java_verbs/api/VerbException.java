package ar.com.dgarcia.java_verbs.api;

/**
 * This type represenst an error on using the verbs
 * Created by kfgodel on 11/08/14.
 */
public class VerbException extends RuntimeException {

    public VerbException(String message) {
        super(message);
    }

    public VerbException(String message, Throwable cause) {
        super(message, cause);
    }

    public VerbException(Throwable cause) {
        super(cause);
    }
}
