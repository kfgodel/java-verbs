package ar.com.dgarcia.java_verbs.api;

import ar.com.dgarcia.java_verbs.api.lambdas.Lambda;

/**
 * This type represents a verb that can be used flexibly according to context
 * Created by kfgodel on 10/08/14.
 */
public interface Verb {
    /**
     * Invokes this verb as a function with the given arguments
     * @param args The arguments passed to this function as context
     * @param <T> The type of expected return
     * @return The return value
     * @throws ar.com.dgarcia.java_verbs.api.VerbException if wrong arguments used
     */
    <T> T apply(Object... args) throws VerbException;

    /**
     * Invokes this verb with hostInstance as an implicit argument (usually the first one)
     * @param hostInstance The instance in which this verb is invoked
     * @param args The extra optional arguments
     * @param <T> The type of expected return
     * @return The return value
     */
    <T> T invokedOn(Object hostInstance, Object... args) throws VerbException;

    /**
     * Changes the definition of this verb with the given lamda
     * @param verbDefinition The code to be run as this verb
     */
    void redefine(Lambda verbDefinition);
}
