package ar.com.dgarcia.java_verbs.api.lambdas;

/**
 * This type represents a function that has 4 or more arguments and has no return value
 * Created by kfgodel on 10/08/14.
 */
public interface TetraOrMoreConsumer<T, U, V, W, X>  {

    /**
     * Performs this operation on the given arguments.
     *
     * @param t the first input argument
     * @param u the second input argument
     * @param v the third input argument
     * @param w the fourth input argument
     * @param args the rest of the optional arguments
     */
    void accept(T t, U u, V v, W w, X... args);

}
