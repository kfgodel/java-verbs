package ar.com.dgarcia.java_verbs.api.lambdas;

/**
 * This type represents a function that accepts 4 or more arguments
 * Created by kfgodel on 10/08/14.
 */
@FunctionalInterface
public interface TetraOrMoreFunction<T, U, V, W, X, R> {

    /**
     * Applies this function to the given arguments.
     *
     * @param t the first function argument
     * @param u the second function argument
     * @param v the third function argument
     * @param w the fourth function argument
     * @param args the rest of the optional args
     * @return the function result
     */
    R apply(T t, U u, V v, W w, X... args);

}
