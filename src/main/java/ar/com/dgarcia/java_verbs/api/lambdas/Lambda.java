package ar.com.dgarcia.java_verbs.api.lambdas;

import ar.com.dgarcia.java_verbs.impl.lambda.LambdaException;

/**
 * This type represents a generic view of all the possible lambdas
 * Created by kfgodel on 11/08/14.
 */
public interface Lambda {
    /**
     * Calls this lambda expression evaluating it with the given arguments.<br>
     *     An exception is thrown if arguments don't match lambda definition
     * @param args The arguments to use in the invocation
     * @param <T> The type of expected result
     * @return The returned value
     * @throws ar.com.dgarcia.java_verbs.impl.lambda.LambdaException if there's an argument mismatch
     */
    <T> T apply(Object... args) throws LambdaException;

    /**
     * @return The minimun number of arguments that this lambda requires to be executed
     */
    int getMinimumArgCount();

    /**
     * @return The maximum number of arguments that this lambda requires. Can be Integer.MAX if this lambda has variable args
     */
    int getMaximumArgCount();
}
