package ar.com.dgarcia.java_verbs.api.lambdas;

/**
 * This type represents a function that has no return value and 3 arguments
 * Created by kfgodel on 10/08/14.
 */
public interface TriConsumer<T, U, V> {

    /**
     * Performs this operation on the given arguments.
     *
     * @param t the first input argument
     * @param u the second input argument
     * @param v the third input argument
     */
    void accept(T t, U u, V v);

}
